# vaspcopy

Collection of Bash scripts that copy or archive important data from one or
multiple VASP calculation directories. Configuration is possible by editing
variables in the scripts.


## `vasptar.sh`
Archives multiple subdirectories with calculations into a single archive.
Calculations are archived in subdirectories with the same name as the
original calculation directory. If subdirectories to copy are not manually
specified, all calculation subdirectories are copied.

Usage:
```bash
vasptar.sh [SUBDIRS_TO_COPY]
```


## `vaspcopy.sh`
Copies important files produced by VASP to the specified directory.
Optionally, the script can create a subdirectory (`SUBDIR`) inside the
destination directory and copy files in the subdirectory.

Usage:
```bash
vaspcopy.sh DESTINATION_DIR [SUBDIR]
```

## `vaspmulticopy.sh`
Runs `vaspcopy.sh` on multiple subdirectories. Calculations are copied in
subdirectories with the same name as the original calculation directory.
If not subdirectories to copy are not manually specified, all calculation
subdirectories are copied.

Usage:
```bash
vaspmulticopy.sh DESTINATION_DIR [SUBDIRS_TO_COPY]
```