#! /bin/bash

# vaspmulticopy.sh
# Version 1.0.0
# Date: 2020-04-28
#
# Runs vaspcopy.sh on multiple subdirectories. Calculations are copied in
# subdirectories with the same name as the original calculation directory.
# If not subdirectories to copy are not manually specified, all calculation
# subdirectories are copied.
#
# Usage:
# vaspmulticopy.sh DESTINATION_DIR [SUBDIRS_TO_COPY]

# Root directory
ROOT=$(pwd)
# Required files to autocopy subdir
REQUIRED_FILES=("POSCAR" "POTCAR" "INCAR" "KPOINTS" "OUTCAR" "CONTCAR"\
                "vasprun.xml" "runvasp.sh")
# Destination directory
DEST=$1
# Array of optional subdirs to copy
COPY_SUBDIRS=()


# Checks if required files exist in the current working directory.
# Returns:
# * 0 if required files exist
# * 1 if they do not, are not readable, or are a directory
check_required_files () {
    for FILE in "${REQUIRED_FILES[@]}"
    do
        if [ -e "$FILE" ]
        then
            if [ -d "$FILE" ]
            then
                echo "'$FILE' in '$(pwd)' is a directory!"
                return 1
            fi
            if [ ! -r "$FILE" ]
            then
                echo "'$FILE' in '$(pwd)' is not readable!"
                return 1
            fi
        else
            return 1
        fi
    done
    # All files present and ok
    return 0
}


# Get absolute path to $DEST
DEST=$(realpath "$DEST")
# Check if $DEST is a directory
if [ ! -d "$DEST" ]
then
    echo "Destination directory '$DEST' does not exist or is not a directory!"
    exit 1
fi

# Remove destination directory from the list of command line args
shift
# Parse optional command line arguments
while [[ $# -gt 0 ]]
do
    COPY_SUBDIRS+=("$1")
    shift
done

# If no optional subdirs were passed, iterate through all subdirs of current
# working directory. Else process only specified subdirs.
if [ ${#COPY_SUBDIRS[@]} -eq 0 ]
then
    # Process all subdirs
    for DIR in */
    do
        cd "$ROOT" || { echo "Could not change directory to '$ROOT'!";\
                        exit 1; }
        if [ -d "$DIR" ]
        then
            cd "$DIR" || { echo "Could not change directory to '$DIR'!";\
                           exit 1; }
            if check_required_files;
            then
                vaspcopy.sh "$DEST" "$DIR"
            else
                echo "Subdirectory '$DIR' does not contain required files."
            fi
        fi
    done
else
    # Process only specified subdirs
    for DIR in "${COPY_SUBDIRS[@]}"
    do
        cd "$ROOT" || { echo "Could not change directory to '$ROOT'!";\
                        exit 1; }
        if [ -d "$DIR" ]
        then
            cd "$DIR" || { echo "Could not change directory to '$DIR'!";\
                           exit 1; }
            if check_required_files;
            then
                vaspcopy.sh "$DEST" "$DIR"
            else
                echo "Subdirectory '$DIR' does not contain required files."
            fi
        fi
    done
fi

