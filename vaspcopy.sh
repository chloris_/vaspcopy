#! /bin/bash

# vaspcopy.sh
# Version 1.0.0
# Date: 2020-04-28
#
# Copies important files produced by VASP (in variable $FILES) to specified
# directory (DESTINATION_DIR). Optionally, the script can create a
# subdirectory (SUBDIR) inside the destination directory and copy files in
# the subdirectory.
#
# Usage:
# vaspcopy.sh DESTINATION_DIR [SUBDIR]

# Files that will be copied
FILES="POSCAR POTCAR INCAR KPOINTS OUTCAR CONTCAR vasprun.xml runvasp.sh"
# Optional files that will be copied if they exist
OPTFILES="komentar.txt"
# Destination directory
DEST=$1
# Subdirectory
SUBDIR=$2

# Instructions
USAGE="Usage:
vaspcopy.sh DESTINATION_DIR [SUBDIR]"

if [ $# == 0 ]
then
  echo "$USAGE"
  exit 1
fi

if [ -d "$DEST" ]
then
  if [ -n "$SUBDIR" ]
  then
    echo ":: Creating subdirectory '$DEST/$SUBDIR'."
    mkdir -p "$DEST/$SUBDIR"
    DEST="$DEST/$SUBDIR"
  fi

  echo ":: Copying VASP files to '$DEST'."
  cp $FILES -t "$DEST"

  for file in $OPTFILES
  do
    if [ -e $file ]
    then
      cp "$file" -t "$DEST"
    fi
  done
else
  echo ":: Directory '$DEST' does not exist."
fi
