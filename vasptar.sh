#! /bin/bash

# vaspmulticopy.sh
# Version 1.0.0
# Date: 2020-08-20
#
# Archives multiple subdirectories with calculations into a single archive.
# Calculations are archived in subdirectories with the same name as the
# original calculation directory. If subdirectories to copy are not manually
# specified, all calculation subdirectories are copied.
#
# Usage:
# vasptar.sh [SUBDIRS_TO_COPY]

# Required files to autocopy subdir
REQUIRED_FILES=("POSCAR" "POTCAR" "INCAR" "KPOINTS" "OUTCAR" "CONTCAR"\
                "vasprun.xml" "runvasp.sh")
# Optional files that will be copied if they exist
OPTIONAL_FILES=("XDATCAR" "komentar.txt")
# Array of optional subdirs to copy
COPY_SUBDIRS=()
# Archive name
ARCHIVE_NAME="pack.tar.gz"
# Archive creation command
ARCHIVE_CREATE="tar -czf \"$ARCHIVE_NAME\""

# Used as function parameter in archive_dir ()
DIR_TO_ARCHIVE=""
# A list of files passed as parameter to the archival program. Used because tar
# does not allow updating compressed archives.
FILES_TO_ARCHIVE=""


# Checks if required files exist in the subdirectory $DIR_TO_ARCHIVE of the
# current working directory.
# Returns:
# * 0 if required files exist
# * 1 if they do not, are not readable, or are a directory
function check_required_files {
    for FILE in "${REQUIRED_FILES[@]}"
    do
        # Assuming dir name ends with '/'
        FILE="$DIR_TO_ARCHIVE$FILE"
        if [ -e "$FILE" ]
        then
            if [ -d "$FILE" ]
            then
                echo "'$FILE' in '$(pwd)' is a directory!"
                return 1
            fi
            if [ ! -r "$FILE" ]
            then
                echo "'$FILE' in '$(pwd)' is not readable!"
                return 1
            fi
        else
            return 1
        fi
    done
    # All files present and ok
    return 0
}


# Queues files from $DIR_TO_ARCHIVE for archival. Required files are queued in
# any case. Optional files are only queued if they exist and are readable.
function queue_dir_for_archival {
    for FILE in "${REQUIRED_FILES[@]}"
    do
        # Assuming dir name ends with '/'
        FILES_TO_ARCHIVE+=" \"$DIR_TO_ARCHIVE$FILE\""
    done
    for FILE in "${OPTIONAL_FILES[@]}"
    do
        # Assuming dir name ends with '/'
        FILE_PATH="$DIR_TO_ARCHIVE$FILE"
        if [ -f "$FILE_PATH" ]
        then
            FILES_TO_ARCHIVE+=" \"$FILE_PATH\""
        fi
    done

    echo ":: Queued dir '$DIR_TO_ARCHIVE' for archival"
}


# Check if archive already exists
if [ -e $ARCHIVE_NAME ]
then
    # Delete the old archive so the new one gets created in its place
    echo ":: Deleting old archive '$ARCHIVE_NAME'"
    rm "$ARCHIVE_NAME"
fi

# Remove destination directory from the list of command line args
shift
# Parse optional command line arguments
while [[ $# -gt 0 ]]
do
    COPY_SUBDIRS+=("$1")
    shift
done

# If no optional subdirs were passed, iterate through all subdirs of current
# working directory. Else process only specified subdirs.
if [ ${#COPY_SUBDIRS[@]} -eq 0 ]
then
    # Process all subdirs
    for DIR in */
    do
        if [ -d "$DIR" ]
        then
            DIR_TO_ARCHIVE="$DIR"
            if check_required_files;
            then
                queue_dir_for_archival
            else
                echo "Subdirectory '$DIR' does not contain required files."
            fi
        fi
    done
else
    # Process only specified subdirs
    for DIR in "${COPY_SUBDIRS[@]}"
    do
        if [ -d "$DIR" ]
        then
            DIR_TO_ARCHIVE="$DIR"
            if check_required_files;
            then
                queue_dir_for_archival
            else
                echo "Subdirectory '$DIR' does not contain required files."
            fi
        fi
    done
fi

# Create the archive
if [ -n "$FILES_TO_ARCHIVE" ]
then
    echo ":: Creating new archive '$ARCHIVE_NAME'"
    eval "$ARCHIVE_CREATE" "$FILES_TO_ARCHIVE" ||\
        { echo "Archival failed!"; exit 1; }
    echo ":: Archival complete!"
else
    echo "No files to archive!"
fi
